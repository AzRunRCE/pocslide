import React, { Component } from "react"
import {View,Keyboard,Dimensions,Animated,Alert,StyleSheet,Picker, Text, ScrollView,TextInput, Image, TouchableOpacity, FlatList, ActivityIndicator, KeyboardAvoidingView} from "react-native"
import styles from "./styles"
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default class StepsView extends Component {

    constructor(props){
        super(props)
        this.state = {
            isLoading:false,
            show: false,
            x: new Animated.Value(0),
            step:0,
            maxStep:this.props.children.length,
        };
  
    }



    goPrevious = () => {
        if (this.state.step == 0)
        {
          this.props.navigation.goBack();
        } else{
          Animated.spring(this.state.x, {
            toValue: windowWidth * -(this.state.step -1),
            useNativeDriver: true,
          }).start();
          this.setState({step: this.state.step -1},() =>{
            this.props.onSlide?.(this.state.step);
          })
        }
    };

    setDate = () => {
        this.slide(3);
    }
    
    slide = (index) => {
        var nextStep =this.state.step +1;
        if (index != null)
            nextStep=index;
        Animated.spring(this.state.x, {
            toValue: windowWidth * -(nextStep),
            useNativeDriver: true,
        }).start();
        this.setState({step: nextStep},() =>{
            this.props.onSlide?.(this.state.step);
        })
    };

    pagination = () => {
    var dots = [];
    for(var i=0; i < this.state.maxStep; i++){
        if (i == this.state.step)
        dots.push(<Text key={i+"cross"} style={{color: 'white',marginHorizontal:1}}>X</Text>)
        else 
        dots.push(<Text key={i+"dot"} style={{color: 'white',marginHorizontal:1}}>O</Text>)
    }

        
    return (<View style={{flexDirection:"row", alignItems:"center", justifyContent:"space-between", marginBottom:10, marginTop:35}}>
            <TouchableOpacity  style={[styles.row,{flex:3}]} onPress={this.goPrevious}>
                <Text style={{color: 'white',marginHorizontal:10}}>{"<"}</Text>
                <Text style={{color: 'white'}}>Précedent</Text>
            </TouchableOpacity>
            <View style={[styles.row,{flex:3, justifyContent:"center"}]} >
                {dots}
            </View>
            <View style={[styles.row,{flex:3}]}>
            </View>
        </View>)
    }

    render(){
        const finalFields = this.props.children.map((child, index) => {
            if (this.state.step == index){
                return child;
            }
            else 
            {
               return (<View key={index+"text"} style={{paddingHorizontal: 10,width:"100%", backgroundColor:"#003366"}}></View>);
            }
          });
        return (<View style={{flex:1, backgroundColor:"#003366"}}>
                    <Animated.View us style={{transform:[{translateX: this.state.x}],flexDirection:"row"}}  >
                        {finalFields}
                    </Animated.View>
                    {this.pagination()}
                </View>)
    }

}
  