import {StyleSheet, Dimensions} from "react-native"
const { width } = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    backgroundColor: "#003366"
  },
  topBar: {
    backgroundColor: "white",
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "flex-end",
    padding: 16
  },
  teamLogo: {
    width: 70,
    height: 70
  },
  teamInfo: {
    flexDirection: "row",
    alignItems: "center"
  },
  teamName: {
    fontSize: 16,
    fontWeight:'bold',
    marginTop:10
  },
  teamNameOut:{
    fontSize: 13,
    marginTop:10,
    padding:2,
    borderColor: "#e4e4e4",
    borderWidth: 1,
    borderRadius: 8,
    textAlign:'center'
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    width: 64,
    borderRadius: 32,
    backgroundColor: "#eee"
  },
  image_ad: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 84,
    width: 84,
    borderRadius: 32
  },
  InputContainer :{
      justifyContent: 'center',
      flex:1,
      margin: 15
},
title:{
  fontSize: 15, textAlign: "center", margin: 4, marginBottom: 24, width: "75%"
},
block_title:{
  fontSize: 24, textAlign: "center", justifyContent:"center", margin: 4, marginTop: 24,marginBottom:35, color:"white", fontWeight:"600"
},
block_text:{
   textAlign: "center", justifyContent:"center", margin: 4, color:"white"
},
searchBar: {
  backgroundColor: "#f6f6f6",
  flexDirection: "row",
  alignItems: "center",
  borderRadius: 10
},
buttonWhite: {
  backgroundColor: "#f6f6f6",
  justifyContent:"center",
  alignItems: "center",
  borderRadius: 10
},
searchInput: {
  color: 'gray',
  textAlignVertical:"center",
  height: 32,
  paddingHorizontal: 12,
  paddingVertical: 4,
  marginVertical:10    ,
  flex: 1,
  // borderRadius: 4
},
searchIcon: {
  margin: 0,
  padding: 0,
  height: 24
},
  input: {
    borderColor: "#e4e4e4",
    borderWidth: 1,
    borderRadius: 8,
    height: 35,
    width: "75%",
    color: "#e4e4e4",
    fontSize: 14,
    padding:10
  },
  footer: {
    width: "100%",
    alignItems: "center"
  },
  control_category_score:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding:4,
    width:200,
    marginTop: 8,
    backgroundColor: "#003366",
  },
  button: {
    backgroundColor: "#003366",
    color: "#fff",
    borderRadius: 8,
    paddingHorizontal: 60,
    paddingVertical: 8,
    marginVertical: 8,
    alignItems: "center",
    fontSize:16,
    fontWeight:'bold'
  },
  buttonGray: {
    backgroundColor: "#eee",
    color: "#fff",
    borderRadius: 8,
    paddingHorizontal: 60,
    paddingVertical: 8,
    marginVertical: 8,
    alignItems: "center",
    fontSize:16,
    fontWeight:'bold'
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  spaceBetween: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
  },
  videoContainer: {
    flex: 1,
    backgroundColor: '#000',
},
backgroundVideo: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
},
dateButton: {
  backgroundColor: "#F6F4F7",
  borderRadius: 15,
  paddingHorizontal: 14,
  paddingVertical: 12,
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
},
dateText: {
  color: "black",
  fontSize: 16,
  marginHorizontal: 6
},
toggleButton:{
  backgroundColor : '#003366', 
  color:"white"
},
teamLogo: {
  height: 38,
  width: 38,
  margin:3,
  marginRight: 8,
  
},
})
